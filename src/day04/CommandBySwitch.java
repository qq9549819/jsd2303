package day04;

import java.util.Scanner;

public class CommandBySwitch {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入-1退出");
        while (true) {
            int b = sc.nextInt();
            if (b == -1) {
                break;
            } else {
                System.out.println("请输入数字,至于是什么,你猜");
                int a = sc.nextInt();
                switch (a) {
                    case 1:
                        System.out.println("存款业务...");
                        break;
                    case 2:
                        System.out.println("取款业务...");
                        break;
                    case 3:
                        System.out.println("查询余额...");
                        break;
                    case 4:
                        System.out.println("转账业务...");
                        break;
                    case 5:
                        System.out.println("退卡...");
                        break;
                    default:
                        System.out.println("输入错误");
                        break;
                }
            }
        }
    }
}