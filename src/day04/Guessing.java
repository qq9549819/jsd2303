package day04;

import java.util.Scanner;

public class Guessing {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = (int) (Math.random() * 100 + 1);
        //11
        //13
        //10
        //11

        //22
        //20
        //23
        int guess;
        do {
            System.out.println("你猜一猜嘛~" + num);
            guess = sc.nextInt();
            if (guess > num) {
                System.out.println("猜大了");
            } else if (guess < num) {
                System.out.println("猜小了");
            } else {
                System.out.println("猜对了");
            }
        } while (guess != num);
























        /*
        System.out.println("你猜一猜嘛~"+num);
        int guess = sc.nextInt();
        while (guess != num) {
            if (num > guess) {
                System.out.println("猜小了,你再猜一猜嘛~");
            } else {
                System.out.println("猜大了,你再猜一猜嘛~");
            }
            guess = sc.nextInt();
        }
        System.out.println("恭喜您,猜对了,奖励个大嘴巴子");

         */
    }
}
//while套while,可以多次玩,猜的数字比被猜的数字小于或者大于10以内提示