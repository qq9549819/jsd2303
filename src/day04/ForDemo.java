package day04;

public class ForDemo {
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += i;
        }
        System.out.println("sum=" + sum);
        //        sum=0
        //i=1     sum=1
        //i=2     sum=1+2
        //i=3     sum=1+2+3
        //i=4     sum=1+2+3+4
        //..................
        //i=100   sum=1+2+3+4+...+100































        /*
        for (int num = 1; num <= 9; num++) {
            System.out.println(num + "*9=" + num * 9);
        }
        //num=1  true  输出
        //num=2  true  输出
        //num=3  true  输出
        //num=4  true  输出
        //num=5  true  输出
        //num=6  true  输出
        //num=7  true  输出
        //num=8  true  输出
        //num=9  true  输出
        //num=10 false 停止,不输出

        for (int num = 1; num <= 9; num += 2) {
            System.out.println(num + "*9=" + num * 9);
        }
        //num=1   true  输出
        //num=3   true  输出
        //num=5   true  输出
        //num=7   true  输出
        //num=9   true  输出
        //num=11  false 停止,不输出
        for (int num = 9; num >= 1; num--) {
            System.out.println(num + "*9=" + num * 9);
        }




























       /* for (int times = 0; times < 5; times++) {
            System.out.println("行动是成功的阶梯");
        }
        System.out.println("继续执行");
    }
}

        */
/*
times=0   true   输出
times=1   true   输出
times=2   true   输出
times=3   true   输出
times=4   true   输出
times=5   false  for循环结束
输出继续执行
 */
    }
}