package day04;

public class WhileDemo {
    public static void main(String[] args) {
        int times = 0;
        while (times < 5) {
            System.out.println("行动是成功的阶梯");
            times++;
        }
        int num = 1;
        while (num <= 9) {
            System.out.println(num + "*9=" + num * 9);
            num++;
        }
        int num1 = 1, num2 = 1;
        while (num1 <= 9) {
            while (num2 <= num1) {
                System.out.print(num2 + "*" + num1 + "=" + num1 * num2 + " ");
                num2++;
            }
            System.out.println();
            num2 = 1;
            num1++;
        }
    }
}
