package day03;

import java.util.Scanner;

public class IfDemo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int b = (int) (Math.random() * 100 + 1);
        while (true) {
            int a = sc.nextInt();
            if (a > b) {
                System.out.println("猜大了");
            } else if (a < b) {
                System.out.println("猜小了");
            } else {
                System.out.println("猜对了");
                break;
            }
        }

    }
}
