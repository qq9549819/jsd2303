package day03;

public class ExerciseDemo {
    public static void main(String[] args) {
        //1.
        System.out.println(5 % 2);
        System.out.println(15 % 3);
        System.out.println(2 % 10);

        int a = 1;
        a++;
        System.out.println(a);
        a--;
        System.out.println(a);

        int a1 = 1;
        int a2 = a1++;
        System.out.println(a2);
        int a3 = a2++;
        System.out.println(a3);

        //2.
        int b = 1, b1 = 2, b2 = 1;
        System.out.println(b > b1);
        System.out.println(b < b2);
        System.out.println(b1 = b2);
        System.out.println(b >= b1);
        System.out.println(b2 <= b2);
        System.out.println(b1 != b2);
        System.out.println(b == b1 + b2);

        //3.
        int c1 = 1, c2 = 2, c3 = 1;

        System.out.println(c2 > c1 && c1 == c2);
        System.out.println(c1 > c2 && c1 == c3);
        System.out.println(c1 == c3 && c2 > c1);
        System.out.println(c2 < c1 && c1 > c3);

        System.out.println(c1==c3||c2<c3);
        System.out.println(c1>c3||c2>c1);
        System.out.println(c1==c3||c2>c1);
        System.out.println(c2<c3||c1>c3);

        System.out.println(!(c1>c2));
        System.out.println(!(c2>c3));

        System.out.println(c2<c1&&c2++>c3);
        System.out.println(c1<c2||c1++>c3);

        //4.
        int d=33;
        d+=3;
        d-=3;
        d*=3;
        d/=3;
        d%=3;

        short s=10;
        s+=10;

        //5.
        int age=18;
        String name="wy";
        System.out.println("我的名字是"+name+",年龄为"+age);

        System.out.println(1+1+1+"");
        System.out.println(1+1+""+1);
        System.out.println(1+""+1+1);
        System.out.println(""+1+1+1);

        int c=66;
        int flag=c>33?1:0;

        int e1=4;
        int e2=6;
        int e=e1>e2?e1:e2;
        System.out.println("最大值为:"+e);

        //6.
        int money=600;
        if (money>=500){
            money*=0.8;
        }
        int grade=77;
        if (grade>=0 &&grade<=100){
            System.out.println("成绩合法");
        }
        if (money>=500){
            money*=0.8;
        }else{
            money*=0.9;
        }
        if (money>=2000){
            money*=0.5;
        }else if (money>=1000){
            money*=0.7;
        }else if (money>=500){
            money*=0.8;
        }else{
            money*=0.9;

            System.out.println("123");
        }

    }
}