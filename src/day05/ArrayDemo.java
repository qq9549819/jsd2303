package day05;

import java.util.Arrays;
import java.util.Random;

public class ArrayDemo {
    public static void main(String[] args) {
        Random rand=new Random();
        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(10);
            System.out.println(arr[i]);
        }
        Arrays.sort(arr);//Arrays.sort   Arrays.sort

        System.out.println("排序后");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        System.out.println("倒序");
        for (int i = arr.length - 1; i >= 0; i--) {
            System.out.println(arr[i]);
        }

























        /*
        //声明整数型数组a,包含五个元素,每个元素都是int类型,默认值为0

        int[] a = new int[5];
        //声明浮点型数组d,包含20个元素,每个元素都是double类型,默认值为0.0
        double[] d = new double[20];
        //声明布尔类型数组b,包含26个元素,每个元素都是boolean类型,默认值为false
        boolean[] b = new boolean[26];
        //声明字符型数组c,包含3个元素,每个元素都是char类型,默认值为0
        char[] c = new char[3];
        //声明长整型数组l,包含400个元素,每个元素都是long类型,默认值为0
        long[] l = new long[400];

        int[] arr1 = new int[3];
        int[] arr2 = {2, 5, 8};
        int[] arr3 = new int[]{2, 5, 8};
        int[] arr4;
        arr4 = new int[]{2, 5, 8};

        int[] arr5 = new int[5];
        int[] arr6 = {1, 3, 5};
        int[] arr7 = new int[]{3, 7, 0};
        int[] arr8;
        arr8 = new int[]{1, 5, 8};

        int[] arr = new int[3];
        System.out.println("数组长度:" + arr.length);
        System.out.println(arr[0]);
        arr[0] = 100;
        arr[1] = 200;
        arr[2] = 300;
        System.out.println(arr[arr.length - 1]);


        int[] arr9 = new int[6];
        System.out.println("数组长度为:" + arr9.length);
        System.out.println(arr9[0]);
        arr9[0] = 100;
        arr9[1] = 200;
        arr9[2] = 300;
        arr9[3] = 400;
        arr9[4] = 500;
        arr9[5] = 600;
        //arr9[6]=700;   运行时发生数组下标越界异常
        System.out.println(arr9[2]);
        System.out.println(arr9[arr9.length - 1]);

          int[] as=new int[10];
          for (int i=0;i<as.length;i++){
              as[i]=100;
              System.out.println(as[i]);
          }
*/


    }
}
