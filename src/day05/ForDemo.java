package day05;

public class ForDemo {
    public static void main(String[] args) {
        for (int num = 1; num <= 9; num++) {
            if (num == 4) {
                continue;
            } else if (num == 8) {
                break;
            }
            System.out.println(num + "*9=" + num * 9);






        /*
        for (int num = 1; num <= 9; num++) {
            if (num == 4) {
                System.out.println(4);
            }else if (num==8){
                break;
            }
            System.out.println(num + "*9=" + num * 9);
        }

         */

        }
    }
}
