package object;

public class IntegerDemo {
    public static void main(String[] args) {
        Integer i1=new Integer(5);
        Integer i2=new Integer(5);
        System.out.println(i1==i2);
        System.out.println(i1.equals(i2));

        Integer i3=Integer.valueOf(5);
        Integer i4=Integer.valueOf(5);
        System.out.println(i3==i4);
        System.out.println(i3.equals(i4));

        int max=Integer.MAX_VALUE;
        int min=Integer.MIN_VALUE;
        System.out.println("最大值为:"+max+"最小值为:"+min);

        String s1="39";
        int age=Integer.parseInt(s1);
        System.out.println(age);
    }
}
