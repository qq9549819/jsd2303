package day07;

import java.util.Random;

public class VerificationCode {
    public static void main(String[] args) {
        String code = generateVerificationCode(6);
        System.out.println(code);
    }

    public static String generateVerificationCode(int len) {
        String code = "";
        Random rand = new Random();
        char[] chs = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
                'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2',
                '3', '4', '5', '6', '7', '8', '9'};
        for (int i = 0; i < len; i++) {
            int index = rand.nextInt(chs.length);
            code += chs[index];
        }
        return code;
    }
}
