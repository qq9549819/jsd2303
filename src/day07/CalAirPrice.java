package day07;

import java.util.Scanner;

public class CalAirPrice {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入机票原价:");
        double price = scan.nextDouble();
        System.out.println("请输入月份:");
        int month = scan.nextInt();
        System.out.println("请选择舱位: 1.头等舱 2.商务舱 3.经济舱");
        int type = scan.nextInt();
        double finalPrice = calFinalPrice(price, month, type);
        System.out.println("机票最终价格为:" + finalPrice);
    }
    public static double calFinalPrice(double price, int month, int type) {
        if (month>=5&&month<=10){
            switch (type){
                case 1:
                    price*=0.9;
                    break;
                case 2:
                    price*=0.85;
                    break;
                case 3:
                    price*=0.8;
                    break;
            }
        }else {
            switch (type){
                case 1:
                    price*=0.7;
                    break;
                case 2:
                    price*=0.65;
                    break;
                case 3:
                    price*=0.6;
                    break;
            }
        }
            return price;
    }
}
