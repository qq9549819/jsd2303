package day07;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class CalTotalAvg {
    public static void main(String[] args) {
        double[] scores = inputData(6); //1)录入评委的评分
        double avg = calAvg(scores); //2)计算平均分
        System.out.println("平均分为:" + avg);
    }

    public static double[] inputData(int count) {
        double[] scores = new double[count]; //评分数组
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < scores.length; i++) {
            System.out.println("请输入第" + (i + 1) + "个评委的评分");
            scores[i] = scan.nextDouble();
        }
        return scores;
    }

    public static double calAvg(double[] scores) {
        double total = calTotal(scores);
        return total / (scores.length - 2);
    }

    public static double calTotal(double[] scores) {
        Arrays.sort(scores);
        double avg = 0;
        for (int i = 1; i < scores.length - 1; i++) {
            avg += scores[i];
        }
        return avg;
    }
}