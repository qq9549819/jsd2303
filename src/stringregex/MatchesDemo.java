package stringregex;

public class MatchesDemo {
    public static void main(String[] args) {
        /*
        [a-zA-Z0-9_]+@[a-zA-Z0-9]+(\.[a-zA-Z]+)+
         */
        String email = "1@q.c";
        String regex = "[a-zA-Z0-9_]+@[a-zA-Z0-9]+(\\.[a-zA-Z]+)+";
        boolean match = email.matches(regex);
        if (match) {
            System.out.println("是正确的邮箱格式");
        } else {
            System.out.println("错误的邮箱格式");
        }
    }
}
