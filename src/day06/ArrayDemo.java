package day06;

import java.util.Arrays;

public class ArrayDemo {
    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4, 5, 6, 10, 7, 98, 8, 65, 9};
        int[] b = new int[10];
        int[] c = Arrays.copyOf(a, 10);
        a = Arrays.copyOf(a, a.length +10);
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }

        System.out.println("12334536");
        for (int i = 0; i < c.length; i++) {
            System.out.println(c[i]);
        }
        System.out.println("12334536");
        System.arraycopy(a, 3, b, 1, 8);
        for (int i = 0; i < b.length; i++) {
            System.out.println(b[i]);
        }
    }
}
