package day06;

import java.util.Random;

public class MethodDemo {
    public static void main(String[] args) {
        say();
        say("wy");
        say("wy", 24);
        int max = max(19, 2);
        int[] arr = generateArray(10, 100);
        traverse(arr);
        System.out.println("继续执行 ");
    }

    public static void traverse(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static int[] generateArray(int length, int num) {
        Random rand = new Random();
        int[] arr = new int[length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(num);
        }
        return arr;
    }


    public static void say(String name, int age) {
        if (age > 50) {
            return;
        }
        System.out.println(name + age);
    }

    public static void say(String name) {
        System.out.println("Hello" + name + "word");
    }

    public static void say() {
        System.out.println("Hello Word");
    }

    public static int max(int a, int b) {
        return Math.max(a, b);
    }
}


