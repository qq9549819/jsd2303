package day06;

import java.util.Arrays;

public class MaxOfArrayDemo {
    public static void main(String[] args) {
        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
            System.out.println(arr[i]);
        }
        int max = arr[0], min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            // if (max < arr[i]) {
            //max = arr[i];
            // }
            //min=min<arr[i]?min:arr[i];
            min = Math.min(min, arr[i]);
            max = Math.max(max, arr[i]);
        }
        System.out.println("max=" + max);
        System.out.println("min=" + min);
        System.out.println("--------------分割------------");
        arr = Arrays.copyOf(arr, arr.length + 2);
        arr[arr.length - 2] = min;
        arr[arr.length - 1] = max;
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);


        }
    }
}
