package ooday01;

public class StudentTest {
    public static void main(String[] args) {
        Student zs = new Student("张三", 24, "jsd2303", "001");
        zs.study();
        zs.sayHi();
        zs.playWith("李四");

        Student ls = new Student("李四", 24, "jsd2303", "002");
        ls.study();
        ls.sayHi();
        ls.playWith(zs.name);

        Student ww = new Student(null, 44, null, null);
        ww.study();
        ww.sayHi();
        ww.playWith(ls.name);


    }
}
