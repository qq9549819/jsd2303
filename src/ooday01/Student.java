package ooday01;

public class Student {
    String name;
    int age;
    String className;
    String stuId;

    Student(String name, int age, String className, String stuId) {
        this.name = name;
        this.age = age;
        this.className = className;
        this.stuId = stuId;
    }

    void study() {
        System.out.println(this.name + "在学习");
    }

    void sayHi() {
        System.out.println("大家好,我叫:" + this.name + ",今年" + this.age + "岁了,所在班级为" + className + "学号为:" + stuId);
    }

    void playWith(String anotherName) {
        System.out.println(this.name + "正在和" + anotherName + "一起玩耍");
    }
}
