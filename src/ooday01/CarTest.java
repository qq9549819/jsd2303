package ooday01;

public class CarTest {
    public static void main(String[] args) {
        Car AstonMartin = new Car("阿斯顿马丁", "蓝色", 100000);
        AstonMartin.start();
        AstonMartin.run();
        AstonMartin.stop();

        Car Lamborghini = new Car("兰博基尼", "红色", 800000);
        Lamborghini.start();
        Lamborghini.run();
        Lamborghini.stop();

        Car Tesla = new Car("特斯拉", "黑色", 300000);
        Tesla.start();
        Tesla.run();
        Tesla.stop();
    }
}
