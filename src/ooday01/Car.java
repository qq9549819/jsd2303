package ooday01;

public class Car {
    String brand;
    String color;
    double price;

    Car() {

    }

    Car(String brand, String color, double price) {
        this.brand = brand;
        this.color = color;
        this.price = price;
    }

    void start() {
        System.out.println("一辆" + brand + "启动了");
    }

    void run() {
        System.out.println("一辆" + color + "的" + brand + "跑起来了");
    }

    void stop() {
        System.out.println("一辆价值" + price + "的" + color + "的" + brand + "停了下来");
    }
}
