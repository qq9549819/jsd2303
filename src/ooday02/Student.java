package ooday02;

public class Student extends Person {
    String className;
    String stuId;

    Student(String name, int age, String address, String className, String stuId) {
        super(name,age,address);
        this.className = className;
        this.stuId = stuId;
    }

    void study() {
        System.out.println(name + "正在" + className + "里学习...");
    }
     void sayHi(){
        System.out.println("大家好,我叫" + name + ",今年" + age + "岁了,我住在" + address+",班级为"+className+",学号为"+stuId);
    }
}
