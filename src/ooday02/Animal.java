package ooday02;

public class Animal {
    String name;
    double age;
    String color;

    Animal(String name, double age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    void drink() {
        System.out.print(age + "岁的" + color + "的" + name + "在");
    }

    void eat() {
        System.out.print(age + "岁的" + color + "的" + name + "在吃");
    }
}
