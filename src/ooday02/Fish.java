package ooday02;

public class Fish extends Animal{
    Fish(String name, double age, String color){
        super(name, age, color);
    }
    void drink() {
        super.drink();
        System.out.println("用嘴喝水");
    }

    void eat() {
        super.eat();
        System.out.println( "鱼食");
    }
}
