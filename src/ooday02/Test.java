package ooday02;

public class Test {
    public static void main(String[] args) {
       Student zs=new Student("张三",24,"吉林","jsd2303","007");
       zs.eat();
       zs.sleep();
       zs.sayHi();
       zs.study();

       Teacher ls=new Teacher("李四",24,"长春",7000);
       ls.eat();
       ls.sleep();
       ls.sayHi();
       ls.teach();

       Doctor ww=new Doctor("王五",2,"朝阳区","主治医师");
       ww.eat();
       ww.sleep();
       ww.sayHi();
       ww.cut();

    }
}
