package ooday02;

public class AnimalTest {
    public static void main(String[] args) {
        Dog d = new Dog("狗", 3, "白色");
        d.eat();
        d.drink();
        d.lookHome();

        Chick c = new Chick("我家哥哥", 2.5, "黑白相间");
        c.eat();
        c.drink();
        c.playBasketball();
        c.layAgg();

        Fish f = new Fish("金龙鱼", 3, "金色");
        f.eat();
        f.drink();
    }
}
