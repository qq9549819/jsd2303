package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class IteratorDemo {
    public static void main(String[] args) {
        Collection c=new ArrayList();
        c.add("one");
        c.add("#");
        c.add("two");
        c.add("#");
        c.add("three");
        c.add("#");
        c.add("four");
        c.add("#");
        c.add("five");
        System.out.println(c);

        Iterator it1=c.iterator();
        for (int i = 0; i < c.size(); i++) {
            if (it1.hasNext()){
                System.out.print(it1.next());
                System.out.print(" ");
            }
        }

        Iterator it=c.iterator();
        while (it.hasNext()){
            String str=(String)it.next();
            System.out.println(str);
            if ("#".equals(str)){
                it.remove();
            }
        }
        System.out.println(c);
    }
}
