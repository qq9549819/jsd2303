package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortPoint {
    public static void main(String[] args) {
        List<Point> list=new ArrayList<>();
        list.add(new Point(13258907,223654));
        list.add(new Point(10000,223556));
        list.add(new Point(191283,2234235));
        list.add(new Point(11230984,223465));
        list.add(new Point(132847235,2345));
        list.add(new Point(1234987,23456));
        list.add(new Point(1235436,2124245));
        System.out.println(list);
        list.sort(new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                // return o1.getX()-o2.getX();
                int len1 = o1.getX() * o1.getX() + o1.getY() * o1.getY();
                int len2 = o2.getX() * o2.getX() + o2.getY() * o2.getY();
                return len1 - len2;
            }
        });
        System.out.println(list);
    }
}
