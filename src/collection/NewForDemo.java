package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class NewForDemo {
    public static void main(String[] args) {
        String[] array = {"one", "two", "three", "four", "five"};
        System.out.println("---for---");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        System.out.println("---new for---");
        for (String str : array) {
            System.out.println(str);
        }

        Collection c = new ArrayList();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        Iterator it = c.iterator();
        System.out.println("---while---");
        while (it.hasNext()) {
            String str = (String) it.next();
            System.out.println(str);
        }
        System.out.println("---new for---");
        for (Object obj : c) {
            String str = (String) obj;
            System.out.println(str);
        }


    }
}
