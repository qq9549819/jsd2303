package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LambdaDemo {
    public static void main(String[] args) {
        List<String> list=new ArrayList<>();
        List<Point> list1=new ArrayList<>();
        Collections.sort(list,(o1, o2) -> o1.length()-o2.length());
        Collections.sort(list1,(o1, o2) -> {
            int len1 = o1.getX() * o1.getX() + o1.getY() * o1.getY();
            int len2 = o2.getX() * o2.getX() + o2.getY() * o2.getY();
            return len1 - len2;
        });

    }
}
