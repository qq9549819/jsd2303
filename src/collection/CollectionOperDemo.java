package collection;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionOperDemo {
    public static void main(String[] args) {
        Collection c1 = new ArrayList();
        c1.add("java");
        c1.add("c++");
        c1.add(".net");
        System.out.println("c1" + c1);

        Collection c2 = new ArrayList();
        c2.add("android");
        c2.add("ios");
        c2.add("java");
        c1.addAll(c2);
        System.out.println("c1" + c1);
        System.out.println("c2" + c2);

        Collection c3 = new ArrayList();
        c3.add("c++");
        c3.add("android");
        c3.add("php");
        System.out.println("c3" + c3);

        boolean contains = c1.containsAll(c3);
        System.out.println("是否包含:" + contains);
/*
        c1.removeAll((c3));
        System.out.println("c1="+c1);
        System.out.println("c3="+c3);

 */
        c1.removeAll(c3);
        System.out.println("c1=" + c1);
        System.out.println("c3=" + c3);
    }
}
