package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class GenericDemo {
    public static void main(String[] args) {
        Collection<Point> c = new ArrayList<>();
        c.add(new Point(1, 2));
        c.add(new Point(2, 3));
        c.add(new Point(3, 4));
        c.add(new Point(4, 5));

        Iterator<Point> it = c.iterator();
        while (it.hasNext()) {
            Point p = it.next();
            System.out.println(p);
        }
        for (Point p :
                c) {
            System.out.println(p);
        }



/*
        Collection<String> c = new ArrayList<>();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");

        Iterator<String> it = c.iterator();
        while (it.hasNext()) {
            String str = it.next();
            System.out.println(str);
        }
        for (String str : c) {
            System.out.println(str);
        }

 */
    }
}
