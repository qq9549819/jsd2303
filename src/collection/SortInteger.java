package collection;

import java.util.*;

public class SortInteger {
    public static void main(String[] args) {
        Random rand=new Random();
        List<Integer> list=new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i, rand.nextInt(100));
        }
        System.out.println("list原始数据:"+list);

        Collections.sort(list);
        System.out.println("list排序后的数据:"+list);

        Collections.reverse(list);
        System.out.println("list反转后的数据"+list);
        System.out.println("list的第一个数据"+list.get(0));
    }
}
