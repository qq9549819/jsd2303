package collection.homework;

import collection.Point;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortPoint1 {
    public static void main(String[] args) {

        List<Point> list = new ArrayList<>();
        list.add(new Point(5,8));
        list.add(new Point(15,60));
        list.add(new Point(100,200));
        list.add(new Point(1,250));
        list.add(new Point(26,3));
        list.add(new Point(13,50));
        System.out.println("list原始数据 :"+list);

        list.sort(new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                return o1.getX()-o2.getX();
            }
        });


        list.sort((o1,o2)->o1.getX()-o2.getX());

        System.out.println("list排序后数据:"+list);
    }
}
