package collection.homework;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortPoint2 {
    public static void main(String[] args) {

        List<collection.Point> list = new ArrayList<>();
        list.add(new collection.Point(5,8));
        list.add(new collection.Point(15,60));
        list.add(new collection.Point(100,200));
        list.add(new collection.Point(1,250));
        list.add(new collection.Point(26,3));
        list.add(new collection.Point(13,50));
        System.out.println("list原始数据 :"+list);

        list.sort(new Comparator<collection.Point>() {
            @Override
            public int compare(collection.Point o1, collection.Point o2) {
                return o1.getX()-o2.getX();
            }
        });


        list.sort((o1,o2)->o1.getX()-o2.getX());

        System.out.println("list排序后数据:"+list);
    }
}
