package ooday04;

/**
 * 演示多态
 */
public class Test {
    public static void main(String[] args) {
//        Master master=new Master();
//        Dog dog=new Dog("小黑",2,"白");
//        Fish fish=new Fish("小金",1,"金");
//        Chick chick=new Chick("小灰",3,"灰");
//        master.feed(dog);
//        master.feed(fish);
//        master.feed(chick);


        Animal[] animals = new Animal[5];
        animals[0] = new Dog("小黑", 2, "白");
        animals[1] = new Dog("小白", 1, "黑");
        animals[2] = new Fish("小金", 1, "金");
        animals[3] = new Fish("小花", 2, "花");
        animals[4] = new Chick("小灰", 3, "灰");
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i].name);
            animals[i].eat();
            animals[i].drink();
            if (animals[i] instanceof Dog) {
                Dog dog = (Dog) animals[i];
                dog.lookHome();

            } else if (animals[i] instanceof Chick) {
                Chick chick = (Chick) animals[i];
                chick.layEggs();
            }
            if (animals[i] instanceof Swim){
                Swim swim=(Swim) animals[i];
                swim.swim();
            }
        }
    }
}

