package ooday04;


interface Coo {
    void swim();
}

public class AnonInnerClassDEmo {
    public static void main(String[] args) {
        Aoo o1 = new Aoo() {
        };

        Boo o2 = new Boo() {
            void show() {
                System.out.println("show");
            }
        };
        o2.show();

        Coo o3 = new Coo() {

            public void swim() {
                System.out.println("swim");
            }
        };
        o3.swim();

    }
}

abstract class Boo {
    abstract void show();
}


abstract class Aoo {

}