package ooday05;

public class StaticVar {

    static int b;
    int a;

    StaticVar() {
        a++;
        b++;

    }

    void show() {
        System.out.println("a=" + a + ",b=" + b);
    }
}
