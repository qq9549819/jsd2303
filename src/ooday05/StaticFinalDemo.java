package ooday05;

public class StaticFinalDemo {
    public static void main(String[] args) {
        System.out.println(Loo.PI);
        System.out.println(Loo.num);
        System.out.println(Loo.COUNT);
    }
}

class Loo {
    public static final int COUNT = 5;
    public static final double PI = 3.14159;
    public static int num = 5;
}