package ooday05;

public class GetSetterDemo {
    public static void main(String[] args) {
        Point p = new Point();
        p.setX(100);
        p.setY(300);
        System.out.println(p.getX()+","+p.getY());
    }
}
