package ooday05;

public class StaticMethod {

    static int b;
    int a;

    static void test() {
        //System.out.println(a);
        System.out.println(StaticMethod.b);
    }

    void show() {
        System.out.println(this.a);
        System.out.println(StaticMethod.b);
    }

    void say() {
        System.out.println(a);
    }

    static int plus(int num1,int num2){
        int num=num1+num2;
        return num;
    }

}
