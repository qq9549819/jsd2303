package ooday03;

public class Dog extends Animal implements Swim{
    Dog(String name, double age, String color) {
        super(name, age, color);
    }

    void drink() {
        super.drink();
        System.out.println("用嘴喝水");
    }

    void eat() {
        System.out.print(age + "岁的" + color + "的" + name + "在吃");
        System.out.println("狗粮");
    }

    void lookHome() {
        System.out.println(age + "岁的" + color + "的" + name + "在看家");
    }
    public void swim(){
        System.out.println(age + "岁的" + color + "的" + name + "在游泳");
    }
}