package ooday03;

public abstract class People {
    String name;
    int age;
    double wage;

    void punchIn() {

    }

    void clockOut() {

    }

    abstract void finishTheWork();
}
