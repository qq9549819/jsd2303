package ooday03;


public class RefArrayDemo {
    public static void main(String[] args) {
        Dog[] dogs = new Dog[3];
        dogs[0] = new Dog("大黄", 1, "黑色");
        dogs[1] = new Dog("小白", 2, "黑色");
        dogs[2] = new Dog("大黑", 1, "白色");
        for (int i = 0; i < dogs.length; i++) {
            System.out.println(dogs[i].name);
        }

        Chick[] chicks = new Chick[3];
        chicks[0] = new Chick("小鸡", 2, "黄色");
        chicks[1] = new Chick("小蓝", 2, "蓝色");
        chicks[2] = new Chick("小鸟", 2, "灰色");
        for (int i = 0; i < chicks.length; i++) {
            System.out.println(chicks[i].age);
        }

        Fish[] fish = new Fish[3];
        fish[0] = new Fish("小金", 1, "金色");
        fish[1] = new Fish("小蓝", 2, "蓝色");
        fish[2] = new Fish("小红", 2, "红色");
        for (int i = 0; i < fish.length; i++) {
            System.out.println(fish[i].color);
        }
    }
}
